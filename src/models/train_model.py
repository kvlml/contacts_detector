import os
import sys

import mlflow

sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
sys.path.append(os.path.join(os.path.dirname(__file__), "../../"))

import gc

import click
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from model_lstm import Simple_LSTM
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
from tqdm import tqdm

import constants
import utils


@click.command()
@click.argument("path_train", type=click.Path(exists=True))
@click.argument("path_val", type=click.Path(exists=True))
@click.argument("path_to_save_models", type=click.Path(exists=True))
@click.argument("model_type", type=str, default="lstm")
@click.argument("n_epochs", type=int)
@click.argument("init_lr", type=float, default=0.03)
@click.argument("batch_size_train", type=int, default=4096)
@click.argument("batch_size_inference", type=int, default=4096)
@click.argument("num_workers", type=int, default=24)
@click.argument("max_len", type=int, default=200)
@click.argument("hidden_dim", type=int, default=32)
@click.argument("dropout", type=float, default=0.1)
@click.argument("embedding_dim", type=int, default=27)
@click.argument("seed", type=int, default=1337)
@click.argument("experiment_name", type=str, default="rnn")
@click.argument("run_name", type=str, default="awesome_run_name")
def main(
    path_train: str,
    path_val: str,
    path_to_save_models: str,
    model_type: str,
    n_epochs: int,
    init_lr: float,
    batch_size_train: int,
    batch_size_inference: int,
    num_workers: int,
    max_len: int,
    hidden_dim: int,
    dropout: float,
    embedding_dim: int,
    seed: int,
    experiment_name: str,
    run_name: str,
):
    params_to_log = dict()
    params_to_log["model_type"] = model_type
    params_to_log["n_epochs"] = n_epochs
    params_to_log["init_lr"] = init_lr
    params_to_log["batch_size_train"] = batch_size_train
    params_to_log["max_len"] = max_len
    params_to_log["hidden_dim"] = hidden_dim
    params_to_log["dropout"] = dropout
    params_to_log["embedding_dim"] = embedding_dim
    params_to_log["seed"] = seed

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    utils.seed_everything(seed=seed, device=device)

    df_train = pd.read_csv(path_train)
    X_train, X_val = train_test_split(df_train, test_size=0.1, random_state=seed)
    del df_train
    gc.collect()

    dataset_train = utils.Dataset_RNN(X_train, mode="train", max_len=max_len)
    dataset_valid = utils.Dataset_RNN(X_val, mode="valid", max_len=max_len)

    X_test = pd.read_csv(path_val)
    dataset_test = utils.Dataset_RNN(X_test, mode="valid", max_len=max_len)

    loader_train = torch.utils.data.DataLoader(
        dataset_train,
        batch_size=batch_size_train,
        shuffle=True,
        num_workers=num_workers,
        drop_last=False,
    )
    loader_valid = torch.utils.data.DataLoader(
        dataset_valid,
        batch_size=batch_size_inference,
        shuffle=False,
        num_workers=num_workers,
        drop_last=False,
    )
    loader_test = torch.utils.data.DataLoader(
        dataset_test,
        batch_size=batch_size_inference,
        shuffle=False,
        num_workers=num_workers,
        drop_last=False,
    )

    if model_type == "lstm":
        model = Simple_LSTM(
            n_vocab=constants.N_VOCAB,
            hidden_dim=hidden_dim,
            embedding_dim=embedding_dim,
            dropout=dropout,
        )
    else:
        raise KeyError(f"Unknown model_type {model_type}")
    model.to(device)

    mlflow.set_experiment(experiment_name)
    mlflow.pytorch.autolog()  # :(
    with mlflow.start_run(run_name=run_name) as run:
        mlflow.log_params(params_to_log)

        criterion = nn.BCEWithLogitsLoss()
        optimizer = optim.Adam(model.parameters(), lr=init_lr)
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, n_epochs)

        mlflow.log_params(
            {"criterion": criterion, "optimizer": optimizer, "scheduler": scheduler}
        )

        for epoch in tqdm(range(n_epochs), desc="epochs"):
            print(f"Epoch {epoch + 1} started")
            print("LR:", optimizer.state_dict()["param_groups"][0]["lr"])
            loss_train = utils.train_func(
                loader_train, model, criterion, optimizer, device, debug=False
            )

            scheduler.step()

            loss_valid_local, preds_local = utils.valid_func(
                loader_valid, model, criterion, device, metric=roc_auc_score
            )
            X_val["preds"] = preds_local

            loss_test, preds = utils.valid_func(
                loader_test, model, criterion, device, metric=roc_auc_score
            )
            X_test["preds"] = preds

            mlflow.log_metrics(
                {
                    "loss_train": loss_train,
                    "loss_valid_local": loss_valid_local,
                    "loss_test": loss_test,
                },
                step=epoch + 1,
            )

            torch.save(
                model.state_dict(),
                f"{path_to_save_models}/epoch{epoch + 1}.pth",
            )

            if device == torch.device("cuda"):
                torch.cuda.empty_cache()

            print("Checkpoint saved!")
            print()
            print("*" * 90)
            print()

        mlflow.pytorch.log_model(model, "model")
        del model
        gc.collect()


if __name__ == "__main__":
    main()
