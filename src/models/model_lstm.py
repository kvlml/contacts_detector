import torch
import torch.nn as nn


class Simple_LSTM(nn.Module):
    def __init__(
        self,
        n_vocab,
        hidden_dim,
        embedding_dim,
        dropout,
    ):
        super(Simple_LSTM, self).__init__()
        self.embeddings = nn.Embedding(n_vocab, embedding_dim)
        self.hidden_dim = hidden_dim

        self.lstm_desc = nn.LSTM(
            embedding_dim,
            hidden_dim,
            dropout=dropout,
            num_layers=2,
            bidirectional=True,
            batch_first=True,
        )

        self.pooling_result_dimension = self.hidden_dim * 2
        self.dim_after_concat = self.pooling_result_dimension * 2

        # self.attention_descs = Attention(self.hidden_dim * 2, batch_first=True)
        self.bn1 = nn.BatchNorm1d(self.dim_after_concat)
        self.dropout1 = nn.Dropout(dropout, inplace=True)
        self.fc1 = nn.Linear(self.dim_after_concat, self.pooling_result_dimension)
        self.dropout2 = nn.Dropout(dropout, inplace=True)
        self.fc2 = nn.Linear(self.pooling_result_dimension, 1)

    def _create_embedding_projection(
        self,
        cardinality,
        embed_size,
        add_missing=True,
        padding_idx=0,
        scale_grad_by_freq=False,
    ):
        add_missing = add_missing * 1
        return nn.Embedding(
            num_embeddings=cardinality + add_missing,
            embedding_dim=embed_size,
            padding_idx=padding_idx,
            scale_grad_by_freq=scale_grad_by_freq,
        )

    def forward(self, descs):
        batch_size = descs.shape[0]

        embedded_descs = self.embeddings(descs)

        states_descs, hh_descs = self.lstm_desc(embedded_descs)

        # lengths_descs = [x_.size(1) for x_ in states_descs]
        # atten_descs, _ = self.attention_descs(states_descs, lengths_descs)
        # atten_descs = atten_descs.view(batch_size, -1)
        rnn_max_pool = states_descs.max(dim=1)[0]
        rnn_avg_pool = states_descs.sum(dim=1) / states_descs.shape[1]

        features = torch.cat([rnn_max_pool, rnn_avg_pool], dim=-1)
        features = self.bn1(features)
        features = self.dropout1(features)
        features = features.view(features.size(0), -1)
        features = self.fc1(features)
        features = self.dropout2(features)
        features = self.fc2(features)

        return features
