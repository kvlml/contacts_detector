import os
import re
import sys

import numpy as np
import torch
from torch.utils.data import Dataset
from tqdm import tqdm

sys.path.append(os.path.join(os.path.dirname(__file__), "../"))
import random

import constants


def seed_everything(seed=1337, device=torch.device("cuda")):
    random.seed(seed)
    os.environ["PYTHONHASHSEED"] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    if device == torch.device("cuda"):
        torch.cuda.manual_seed(seed)
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False


def prepare_string(string):
    try:
        result = string.lower().replace("ё", "е").strip()
    except:
        result = ""
    return result


def get_id_of_token(token):
    if token in set(list(constants.token_to_idx.keys())[1:]):
        idx = constants.token_to_idx[token]
    else:
        idx = 0
    return idx


def get_token_of_id(idx):
    if idx in set(list(constants.idx_to_token.keys())[1:]):
        token = constants.idx_to_token[idx]
    else:
        token = "unknown_token"
    return token


def to_matrix(
    names,
    max_len,
    pad=constants.token_to_idx[" "],
    dtype="int16",
    batch_first=True,
):
    """Casts a list of names into rnn-digestible matrix"""

    max_len = max_len or max(map(len, names))
    names_ix = np.zeros([len(names), max_len], dtype) + pad

    for i in range(len(names)):
        cur_name = names[i]
        cur_name = cur_name[-max_len:]
        line_ix = [
            get_id_of_token(cur_name[c]) for c in range(len(cur_name)) if c < max_len
        ]
        names_ix[i, : len(line_ix)] = line_ix

    if not batch_first:  # convert [batch, time] into [time, batch]
        names_ix = np.transpose(names_ix)

    return names_ix


class Dataset_RNN(Dataset):
    def __init__(self, df, mode, max_len):
        self.df = df.reset_index(drop=True)
        self.mode = mode
        self.max_len = max_len

    def __len__(self):
        return len(self.df)

    def __getitem__(self, index):
        row = self.df.loc[index]
        description = row["description"]
        description = to_matrix([description], max_len=self.max_len)[0]

        if self.mode == "test":
            return torch.tensor(description).long()
        else:
            return torch.tensor(description).long(), torch.tensor(row["is_bad"]).float()


def inference_func(inference_loader, model, device):
    model.eval()

    bar = tqdm(inference_loader)
    preds = list()

    with torch.no_grad():
        for batch_idx, descs in enumerate(bar):
            descs = descs.to(device).long()
            logits = model(descs)
            preds += [logits.detach().cpu()]

    preds = torch.cat(preds).cpu().numpy()
    preds = 1.0 / (1.0 + np.exp(-preds))
    preds = np.concatenate(preds, axis=0)
    return preds


def train_func(train_loader, model, criterion, optimizer, device, debug=False):
    model.train()

    bar = tqdm(train_loader)

    losses = list()
    for batch_idx, (descs, targets) in enumerate(bar):
        descs = descs.to(device).long()
        targets = targets.to(device).float()
        targets = targets.unsqueeze(1)

        if debug and batch_idx == 100:
            print("Debug Mode. Only train on first 100 batches.")
            break

        logits = model(descs)

        loss = criterion(logits, targets)
        loss.backward()
        optimizer.step()
        optimizer.zero_grad()

        losses.append(loss.item())
        # smooth_loss = np.mean(losses[-30:])
        # bar.set_description(f"smooth_loss: {smooth_loss:.5f}")

    loss_train = np.mean(losses)
    print("loss_train:", loss_train)
    return loss_train


def valid_func(valid_loader, model, criterion, device, metric):
    model.eval()

    bar = tqdm(valid_loader)
    PREDS = list()
    TARGETS = list()

    losses = list()
    with torch.no_grad():
        for batch_idx, (descs, targets) in enumerate(bar):
            descs = descs.to(device).long()
            targets = targets.to(device).float()
            targets = targets.unsqueeze(1)

            logits = model(descs)

            loss = criterion(logits, targets)
            losses.append(loss.item())

            PREDS += [logits.detach().cpu()]
            TARGETS += [targets.detach().cpu()]

            # bar.set_description(f"loss: {loss.item():.5f}")

    loss_valid = np.mean(losses)
    print("loss_valid:", loss_valid)

    PREDS = torch.cat(PREDS).cpu().numpy()
    TARGETS = torch.cat(TARGETS).cpu().numpy()

    metric_total = metric(TARGETS, PREDS)
    print("metric_total:", metric_total)

    return loss_valid, PREDS


dict_patterns = dict()
dict_patterns["phone"] = re.compile(
    "(\D|^){1}(\+7|7|8)[^\n0-9]*\d{3}[^\n0-9]*\d{3}[^\n0-9]*\d{2}[^\n0-9]*\d{2}"
)
dict_patterns["email"] = re.compile("[a-zA-Z0-9_\.-]+[@][a-z.-]+[\.][a-z]+")
dict_patterns["tg"] = re.compile(
    "((\s|^)tg|telegram|телега|телегу|телеге|телеграм|телеграме)[\s\(:\|-]+@?[a-zA-Z0-9_\.-]+"
)
dict_patterns["discord"] = re.compile(
    "(discord|(\s|^)ds|дискорд)[\s\(:\|-]+[a-zA-Z0-9_\.-]+[#]\d{4}"
)
dict_patterns["inst"] = re.compile(
    "((\s|^)inst|instagram|инстаграм|инстаграме|инст|инста|инсте)[\s\(:\|-]+@?[a-zA-Z0-9_\.-]+"
)
# dict_patterns['universal_match'] = re.compile("@[a-zA-Z0-9_\.]+") # @...


def find_start_end(string, pattern_name, dict_patterns=dict_patterns):
    seps = [" ", ":", "|", "-", "("]
    patterns_to_postproc = ["tg", "discord", "inst"]
    pattern = dict_patterns[pattern_name]
    positions = list()
    groups = list()
    for match in pattern.finditer(string):
        start, end = match.start(), match.end() - 1
        if pattern_name == "phone":
            starts_of_pattern = ["+7", "7", "8"]
            cur_string = string[start : end + 1]
            start_offset_candidates = [
                cur_string.find(start) for start in starts_of_pattern
            ]
            start_offset_candidates = [
                val for val in start_offset_candidates if val != -1
            ]
            start_offset = min(start_offset_candidates)
            start += start_offset
        elif pattern_name in patterns_to_postproc:
            cur_string = string[start : end + 1]
            for sep in seps:
                cur_string = cur_string.split(sep)[-1]
            start = string.find(cur_string)
        positions.append((start, end))
        groups.append(string[start : end + 1])
    num_of_found = len(positions)
    # if num_of_found == 1:
    #     result = positions[0], num_of_found, groups
    # else:
    #     result = positions, num_of_found, groups
    result = positions
    return result  # , groups
